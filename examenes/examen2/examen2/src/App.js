import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './components/NavBar';
import MiDataTable from './components/TablaP';
import MiDataTablePe from './components/TablaPe';
import MiDataTablePi from './components/TablaPi';
import MiDataTablePr from './components/TablaPr';
import MiDataTablePiu from './components/TablaPiu';
import MiDataTablePriu from './components/TablaPriu';
import MiDataTableSpriu from './components/TablaSpriu';
import Footer from './components/Footer';

function App() {
  // Estado para controlar qué componente se muestra debajo de la barra de navegación
  const [currentPage, setCurrentPage] = useState('home'); // 'home' por defecto

  // Función para cambiar la página actual
  const changePage = (page) => {
    setCurrentPage(page);
  };

  // Función para renderizar el componente correspondiente según la página actual
  const renderComponent = () => {
    switch (currentPage) {
      case 'home':
        return <MiDataTable />;
      case 'tablaPi':
        return <MiDataTablePi />;
      case 'tablaPe':
        return <MiDataTablePe />;
      case 'tablaPr':
        return <MiDataTablePr />;
      case 'tablaPiu':
        return <MiDataTablePiu />;
      case 'tablaPriu':
        return <MiDataTablePriu />;
      case 'tablaSpriu':
        return <MiDataTableSpriu />;
      default:
        return <MiDataTable />; // Si la página no coincide, mostrar por defecto MiDataTable
    }
  };

  return (
    <div className="App">
      <NavBar changePage={changePage} /> {/* Pasar la función changePage como prop */}
      {renderComponent()}
      <Footer />
    </div>
  );
}

export default App;
