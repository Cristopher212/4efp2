// Definición del componente 
export default function Footer (){
    return (
          // Componente de pie de página
        <footer className="bg-body-tertiary text-center text-lg-start">
              {/* División para el contenido del pie de página */}
            <div className="text-center p-3" style={{ backgroundColor: 'rgb(155, 155, 155)' }}>
                © 2020 CopyrightPLC:
                <a className="text-body" href="https://mdbootstrap.com/">MDBootstrap.com</a>
            </div>
        </footer>
    );
}
