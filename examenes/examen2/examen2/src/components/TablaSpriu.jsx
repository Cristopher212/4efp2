import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const MiDataTableSpriu = () => {
  const [userId, setUserId] = useState([]);  // Define el estado 'userId' y la función 'setUserId' para almacenar y actualizar los datos de los usuarios
  const url = 'http://jsonplaceholder.typicode.com/todos';

  // Función asincrónica para obtener y mostrar los datos de los usuarios
  const showData = async () => {
    try {
      const response = await fetch(url); // Realiza una solicitud GET a la URL especificada
      const data = await response.json();
      setUserId(data); 
    } catch (error) {
      console.error('Error fetching data:', error); // Maneja los errores de la solicitud
    }
  };

  useEffect(() => {
    showData();
  }, []);

  // Filtrar los datos para mostrar solo los elementos pendientes
  const pendingTasks = userId.filter(item => !item.completed);

  const columns = [
    {
      name: 'Id',
      selector: row => row.id,
    },
    {
      name: 'UserID',
      selector: row => row.userId,
    }
  ];

  return (
    <DataTable
      title="Tareas Pendientes sin resolver (ID y UserID)"
      columns={columns}
      data={pendingTasks}
      pagination
      customStyles={{
        rows: {
          style: {
            fontSize: '16px',
            backgroundColor: 'rgb(255, 255, 255)',
            textAlign: 'center',
          },
        },
        headRow: {
          style: {
            backgroundColor: 'rgb(1, 75, 160)', // Cambia el color de fondo 
            textAlign: 'center',
          },
        },
      }}
    />
  );
};
export default MiDataTableSpriu;
