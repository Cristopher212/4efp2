export default function NavBar(props) {
  const { changePage } = props;

  return (
    <header className="App">
      {/* Componente de encabezado */}
      <nav className="navbar navbar-expand-lg bg-secondary">
        <div className="container-fluid">
          <a className="navbar-brand" href="#">EXMAMEN2NFL</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          {/* Contenedor del menú de navegación */}
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item" onClick={() => changePage('home')}>
                <a className="nav-link active" aria-current="page" href="#">Inicio</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaP')}>
                <a className="nav-link" href="#">Tabla 1</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaPi')}>
                <a className="nav-link" href="#">Tabla 2</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaPe')}>
                <a className="nav-link" href="#">Tabla 3</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaPr')}>
                <a className="nav-link" href="#">Tabla 4</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaPiu')}>
                <a className="nav-link" href="#">Tabla 5</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaPriu')}>
                <a className="nav-link" href="#">Tabla 6</a>
              </li>
              <li className="nav-item" onClick={() => changePage('tablaSpriu')}>
                <a className="nav-link" href="#">Tabla 7</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}
