import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const MiDataTable = () => {
  const [userId, setUserId] = useState([]);  // Define el estado 'userId' para almacenar los IDs de los usuarios
  const url = 'http://jsonplaceholder.typicode.com/todos';

  // Función asincrónica para obtener y mostrar los IDs de los usuarios cuyo 'completed' sea false
  const showData = async () => {
    try {
      const response = await fetch(url); // Realiza una solicitud GET a la URL especificada
      const data = await response.json();
      
      // Filtrar los datos para mostrar solo los elementos con completed igual a false
      const filteredData = data.filter(item => !item.completed);
      const userIds = filteredData.map(item => item.id);
      
      setUserId(userIds); // Establece los IDs de los usuarios en el estado 'userId'
    } catch (error) {
      console.error('Error fetching data:', error); // Maneja los errores de la solicitud
    }
  };

  useEffect(() => {
    showData();
  }, []);
  
  const columns = [
    {
      name: 'Id',
      selector: row => row,
    },
  ];

  return (
    <DataTable
      title="SOLO IDs"
      columns={columns}
      data={userId}
      pagination
      customStyles={{
        rows: {
          style: {
            fontSize: '16px',
            backgroundColor: 'rgb(255, 255, 255)',
            textAlign: 'center',
          },
        },
        headRow: {
          style: {
            backgroundColor: 'rgb(1, 75, 160)', // Cambia el color de fondo
            textAlign: 'center',
          },
        },
      }}
    />
  );
};

export default MiDataTable;
