import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const MiDataTablePi = () => {
  const [userId, setUserId] = useState([]);  // Define el estado 'userId' y la función 'setUserId' para almacenar y actualizar los datos de los usuarios
  const url = 'http://jsonplaceholder.typicode.com/todos';

  // Función asincrónica para obtener y mostrar los datos de los usuarios
  const showData = async () => {
    try {
      const response = await fetch(url); // Realiza una solicitud GET a la URL especificada
      const data = await response.json();
      setUserId(data); 
    } catch (error) {
      console.error('Error fetching data:', error); // Maneja los errores de la solicitud
    }
  };

  useEffect(() => {
    showData();
  }, []);
  const columns = [
    {
      name: 'Id',
      selector: row => row.id,
    },

    {
        name: 'Titles',
        selector: row => row.title,
    }

  ];

  return (
    <DataTable
      title=" Pendientes (IDs y Titles) "
      columns={columns}
      data={userId}
      pagination
      customStyles={{
        rows: {
          style: {
            fontSize: '16px',
            backgroundColor: 'rgb(255, 255, 255)',
            textAlign: 'center',
          },
        },
        headRow: {
          style: {
            backgroundColor: 'rgb(1, 75, 160)', // Cambia el color de fondo
            textAlign: 'center',
          },
        },
      }}
    />
  );
};

export default MiDataTablePi;
