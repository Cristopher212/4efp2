import { StyleSheet, Image } from "react-native-web";

export default function ImageViewer({ PlaceholderImageSource }) {
    return (
        <Image source={PlaceholderImageSource} style={styles.image} />
    );
}

const styles = StyleSheet.create({
    image: {
        width: 720, 
        height: 440,
        borderRadius: 18,
    },
});
