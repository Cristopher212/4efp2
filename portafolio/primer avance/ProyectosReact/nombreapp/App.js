import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Button from './component/Button'; // Corregido el nombre de la importación
import ImageViewer from './component/ImageViewer'; // Corregido el nombre de la importación

const PlaceholderImage = require('./assets/images/hutao.jpg')

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <ImageViewer PlaceholderImageSource={PlaceholderImage}/> {/* Corregido el nombre del componente */}
      </View>
      <Text style={styles.colorText}>Hello world react</Text> {/* Corregido el texto "word" por "world" */}
      <View style={styles.footerContainer}>
        <Button label={"Elige una foto"}/>
        <Button label={"Elige una foto"}/>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#25292e',
    alignItems: 'center',
    justifyContent: 'center',
  },
  colorText: {
   color: 'red' 
  },
  image:{
    width: 720, 
    height: 440,
    borderRadius: 18,
  },

  imageContainer:{
    flex: 1,
    paddingTop: 58,
  },

  footerContainer:{
    flex: 1 / 3,
    alignItems: 'center'
  },
});
