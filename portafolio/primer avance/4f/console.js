// MENSAJE SIMPLE
// console.log("mensaje")

// let message = "Hola Mundo";
// console.log(message)

//NOMBRAR VARIABLES Y SU VALOR
// var num = 1000
// var flo = 105.56

// console.log(num)
// console.log(flo + num)

// const pi = 3.1415

// console.log(num * pi)


// var name2 = "John";

// var admin = name2

// console.log(admin)


// var array = [1,2,3,4,5,6,7,8,9,0];


// for (let i of array) {
//     console.log(i)
// }


// const string = "free fire";
// for(ch of string){
//     console.log(ch)
// }


// const nombre = ['cris','kevin','lucas','fernando','cris'];

// const uniqueName = new Set(nombre);

// for(let nombre of uniqueName){
//     console.log(nombre);
// }


// const obj = {
//     name: "free fire",
//     age: 7,
//     city: "buenos aires",
//     country: "brasil"
// }


// for(let key of Object.keys(obj)){
//     console.log(key+": "+ obj[key])

// }

// console.log("----------------------")

// for (let key in obj) {
//     console.log(key+": "+ obj[key])
// }

// var i = 1000

// while(i<1000){
//     console.log(i)
//     i+=5
// }

// do {
//     console.log(i)
//     i+=5
// }while(i<1000)

// var x = 100

// if(x > 1000) {

//     console.log("IT IS TRUE")
    
// } else{
//     console.log("IT IS NOT TRUE")
// }


// var animal = "Lion"

// if(animal === "Lion") {
//     console.log("YES, IT IS TRUE")
    
// } else{
//     console.log("NO, IT IS NOT TRUE")
// }


// var Tiger = (animal === "Lion") ? "YES, IT IS TRUE": "NO IT IS NOT TRUE"
// console.log(Tiger)

// var salon = 1

// switch (salon) {
//     case 1:
//         console.log("Salon 1")
//         break;

//     case 2:
//         console.log("Salon 2")
//         break;

//     default:
//         console.log("No se encontro el salon")
//         break;
// }

// var base = 100
// var altura= 55

// function area(base , altura){
//     return (base * altura)/2
// }

// console.log ("El area del triangulo: " + area(base, altura))

// var l = 89
// var w = 23
// var h = 12

// function prism(l){
//     return function(w){
//         return function(h){
//             return l * w * h
//         }
//     }
    
// }
// console.log("El volumen del Prisma es: " + prism(l)(w)(h))


//ESTO ES UNA FUNCUION ANONIMAAAAA INVOCACION INMEDIATA
// const f = (function(msg){
//     console.log("Soy una funcion anonima por lo que no me podras ver en otra parte " + msg)
//     return msg
// })("footoromo");

// console.log(f)


// const fx = function sum(x, y){
//     return x + y 

// }

// const fy = function(g, h){
//     return g + h
// }

// console.log("fx: " +fx(10, 21) + " fy: " + fy(605, 809))

// var sumTwoNumbers = function sum (a, b){
//     return a + b;
// }
// sum(1, 3);

// var say = function (times) {
//     if (times > 0) {
//     console.log('Today is not my day!');
//     say(times - 1);
//     }
//     }

// var sayhello = say
// sayhello(1000)

// function personLogsSomeThings(person, ...msg) {
//     msg.forEach(arg => {
//     console.log(person, 'says', arg);
//     });
//     }

//     personLogsSomeThings('John', 'hello', 'world', 'Money', 'i want to go home', 'i want to sleep ');



// const logArguments = (...args) => console.log(args)
// const list = [1, 2, 3, 4]

// logArguments('a', 'b', 'c','d', ...list)


function personSay(person, ...msg){
    msg.forEach(arg =>{
        console.log(person + " Say: " + arg)
        console.log(person  + " Dice " + arg)
    })

}
personSay("Pepe", "Hola", "Mundo", "React", "JS","Los buenos")




