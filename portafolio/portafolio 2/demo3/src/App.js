import './App.css';
import MiDataTable from './components/Tables';

function App() {
  
  return (
    <div className="App">
      <header className="App-header">
          <MiDataTable/>
      </header>
    </div>
  );
  }

export default App;
