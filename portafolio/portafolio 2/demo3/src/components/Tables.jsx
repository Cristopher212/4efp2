import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const MiDataTable = () => {
  const [users, setUsers] = useState([]);  // Define el estado 'users' y la función 'setUsers' para almacenar y actualizar los datos de los usuarios
  const url = 'https://randomuser.me/api/?results=20';

  // Función asincrónica para obtener y mostrar los datos de los usuarios
  const showData = async () => {
    try {
      const response = await fetch(url); // Realiza una solicitud GET a la URL especificada
      const data = await response.json();
      setUsers(data.results); 
    } catch (error) {
      console.error('Error fetching data:', error); // Maneja los errores de la solicitud
    }
  };

  useEffect(() => {
    showData();
  }, []);
  const columns = [
    {
      name: 'Nombre',
      selector: row => row.userId,
    },
    {
      name: 'País',
      selector: row => row.location.country,
    },
    {
      name: 'Correo Electrónico',
      selector: row => row.email,
    },
  ];

  return (
    <DataTable
      title="Usuarios Aleatorios"
      columns={columns}
      data={users}
      pagination
    />
  );
};

export default MiDataTable;
