import { useState, useEffect } from 'react';
//import './App.css';

// URL de la API para obtener la informacion
const url = "https://api.coindesk.com/v1/bpi/currentprice.json";

export default function Btc2() {
  // Estados para manejar los datos errores
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    // Realizar la solicitud a la API
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        setData(result);
      })
      .catch(error => {
        setIsLoading(false);
        setError(error.toString());
      });
  }, []);
  
  //Manejo de errores
  if (error) {
    return <h4>{error}</h4>;
  }
  //Manejo de carga
  if (isLoading) {
    return (
      <div class="App">
        <h4>Loading Data...</h4>
        <progress value={null} />
      </div>
    );
  }

  return (
    <div class="App">
      <h1>BTC TO USD | EURR | GBP</h1>
      <h3>BTC To USD</h3>
      <div  class="col-2"></div>
      <div style={styles.tableContainer} class="col-8">
        <table class="table table-striped">
          <thead>
            <tr >
              <th>RATE </th>
              <th>RATE FLOAT </th>
              <th>DESCRIPTION </th>
              <th>UPDATED</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{data["bpi"]["USD"].rate}</td>
              <td>{data["bpi"]["USD"].rate_float}</td>
              <td>{data["bpi"]["USD"].description}</td>
              <td>{data["time"].updated}</td>
            </tr>
            <tr>
              <td>{data["bpi"]["GBP"].rate}</td>
              <td>{data["bpi"]["GBP"].rate_float}</td>
              <td>{data["bpi"]["GBP"].description}</td>
              <td>{data["time"].updated}</td>
            </tr>
            <tr>
              <td>{data["bpi"]["EUR"].rate}</td>
              <td>{data["bpi"]["EUR"].rate_float}</td>
              <td>{data["bpi"]["EUR"].description}</td>
              <td>{data["time"].updated}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="col-2"></div>
    </div>
  );
}

// Estilos para el componente
const styles = {
  textProps:{
    alignItems: "center",
    justifyContent: "center",
  },

  tableContainer: {
    margin: "0 auto",
    width: "50%" 
  }
  
  
}

