// Importacion de las imagenes
import Ganyu from '../assets/images/Ganyu.jpg';
import hutao from '../assets/images/hutao.jpg';

// Definición del componente 
export default function Cards (){
    return (
        
        <div style={{ display: 'flex' }}>
                {/*Tarjeta 1 */}        
            <div className="card" style={{width: '18rem', marginRight: '10px', marginLeft: '85px'}}>
                <img src={Ganyu} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">Card title 1</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">An item</li>
                </ul>
                <div className="card-body">
                    <a href="#" className="card-link">Card link</a>
                    <a href="#" className="card-link">Another link</a>
                </div>
            </div>
              {/*Tarjeta 2 */}    
            <div className="card" style={{width: '18rem', marginRight: '10px'}}>
                <img src={hutao} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">Card title 2</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">An item</li>
                </ul>
                <div className="card-body">
                    <a href="#" className="card-link">Card link</a>
                    <a href="#" className="card-link">Another link</a>
                </div>
            </div>
               {/*Tarjeta 3 */}              
            <div className="card" style={{width: '18rem', marginRight: '10px'}}>
                <img src={Ganyu} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">Card title 1</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">An item</li>
                </ul>
                <div className="card-body">
                    <a href="#" className="card-link">Card link</a>
                    <a href="#" className="card-link">Another link</a>
                </div>
            </div>
              {/*Tarjeta 4 */}    
            <div className="card" style={{width: '18rem', marginRight: '10px'}}>
                <img src={hutao} className="card-img-top" alt="..."/>
                <div className="card-body">
                    <h5 className="card-title">Card title 2</h5>
                    <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">An item</li>
                </ul>
                <div className="card-body">
                    <a href="#" className="card-link">Card link</a>
                    <a href="#" className="card-link">Another link</a>
                </div>
            </div>


        </div>
    );
}
