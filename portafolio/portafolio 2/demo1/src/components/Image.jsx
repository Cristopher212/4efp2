import hutao from '../assets/images/hutao.jpg';
import Ganyu from '../assets/images/Ganyu.jpg';
import ganyu2 from '../assets/images/ganyu2.jpeg';

// Definicion del componente 
export default function Image(props){
    return(
        <div>
        <img style={styles.reSize} src={hutao} alt="Hutao"/>
        <img style={styles.reSize} src={Ganyu} alt="Ganyu"/>
        <img style={styles.reSize} src={ganyu2} alt="ganyu2"/>
    </div>
    );
}



// Estilos para las imagenes
const styles = {
    reSize: {
        width: 10,
        height: 100,
        paddingTop:50,
        borderRadius: '50px',
        backgroundColor: 'blue',
        boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    },
};


