//importaciones de los componentes

import logo from './logo.svg';
import './App.css';
//import Barra from './components/Barra';
import NabB from './components/NavB';
import Carrousel from './components/Carrousel'
import Footer from './components/Footer';
import Cards from './components/Cards';
import 'bootstrap/dist/css/bootstrap.min.css';



export default  function App() {
  return (
    <div className="App">
      <NabB/>
      <Carrousel/>
      <Cards/>
      <Footer/>

    </div>
  );
}


